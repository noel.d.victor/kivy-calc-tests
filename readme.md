# Kivy Calculator Tests

After building several projects for clients in kivy, I found it's easy to get a horrible mess of spaghetti code. Especially as the project gets more complicated.

This project is test meant to help me experiment with some design patterns for building more complex apps in kivy, and testing out a new code structure.

I'm still not sure how I feel about the some of the experimental patterns and app structure used in it.

**Warning: Uses ASTEEVAL for calc logic**
- http://newville.github.io/asteval/motivation.html#how-safe-is-asteval


## Setup
Kivy setups can be a massive pain.
I'll eventually add a way to streamline setup.
For now I'd recommend you use a virtualenv or a vm.

Follow the instructions here.

https://kivy.org/docs/installation/installation-linux.html#using-software-packages

Then install the requirements
- remember to install cython first then the requirements

# Screenshot
![screenshot](screenshot.png)
