import style

APP_TITLE = 'Noel Victor\'s Kivy Calculator'

#Load the following KV files usually fragments to be part of root kv


KV_FILES = [
    'kv/partials/__menu_bar.kv',
    'kv/partials/__status_bar.kv',
    'kv/partials/__core.kv',
]

#this should be the kv that glues the partials together
ROOT_KV_FILE = 'kv/main.kv'