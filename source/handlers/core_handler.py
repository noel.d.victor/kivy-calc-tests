from kivy.app import App
from kivy.core.audio import SoundLoader

from asteval import Interpreter

from constants import _STATUS_BAR



class CoreHandler(object):
    """
    This should encapsulate code that represents the core calculator logic of app
    """

    # Function called when equals is pressed
    def calculate(self, calculation):
        app = App.get_running_app()
        print(app.root.manager.current_screen.ids._status_bar.ids._last_calc)
        if calculation:
            try:
                # Solve formula and display it in entry
                # which is pointed at by display
                app.root.manager.current_screen.ids._status_bar.ids._last_calc.text = str(calculation.text)
                aeval = Interpreter()
                txt = calculation.text
                txt = aeval.eval(txt)
                calculation.text = str(txt)

            except Exception as e:
                print(e)
                calculation.text = "Error"
