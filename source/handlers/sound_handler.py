from kivy.core.audio import Sound
from kivy.core.audio import SoundLoader

from style import VOLUME_ICON_HIGH
from style import VOLUME_ICON_LOW
from style import VOLUME_ICON_MUTED


class SoundHandler(object):
    """
    This class should handle generic sound functions
    and should get attached to the main App class

    It exists to isolate and encapsulate sound related stuff related to the app not kivy underlying arch
    """

    def change_sound(self, wid):
        if Sound.volume == 0:
            Sound.volume = .5
            wid.icon = VOLUME_ICON_LOW
        elif Sound.volume == .5:
            Sound.volume = 1
            wid.icon = VOLUME_ICON_HIGH
        else:
            Sound.volume = 0
            wid.icon = VOLUME_ICON_MUTED
