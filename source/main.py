# -*- coding: utf-8 -*-
from kivy.app import App
from kivy.lang.builder import Builder

from settings import KV_FILES
from settings import APP_TITLE
from settings import ROOT_KV_FILE

from handlers.sound_handler import SoundHandler
from handlers.core_handler import CoreHandler

#load all the custom widgets, these are used even if a ide says not

from custom_widgets import MainScreenManager
from custom_widgets import MainLayout
from custom_widgets import AppRootWidget
from custom_widgets import Button2
from custom_widgets import StatusBar

class CalcApp(App):
    sound_handler = SoundHandler()
    core_handler = CoreHandler()

    def load_kv(self, filename=None):
        #THIS IS CRITICAL FOR MY CUSTOM APP LOADING STYLE
        root = None
        for item in KV_FILES:
            Builder.load_file(item)
        root = Builder.load_file(ROOT_KV_FILE)
        if root:
            self.root = root
        return True

    def build(self):
        self.title = APP_TITLE
        main = AppRootWidget()
        return main


if __name__ == '__main__':
    CalcApp().run()