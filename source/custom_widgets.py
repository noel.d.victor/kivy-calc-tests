from kivy.app import App

from kivy.core.audio import SoundLoader

from kivy.uix.actionbar import ActionButton
from kivy.uix.button import Button
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.gridlayout import GridLayout

from kivy.uix.screenmanager import ScreenManager
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.actionbar import ActionBar

from style import BUTTON_CLICK_FX

from kivy.properties import ObjectProperty

class AppRootWidget(AnchorLayout):
    """
    This is mainly so menu is above the screens
    """
    manager = ObjectProperty(None)


# The following get initialized here and are used in the kv
class MainScreenManager(ScreenManager):
    pass


class MainLayout(BoxLayout):
    pass



class Button2(Button):
    def on_press(self):
        sound = SoundLoader.load(BUTTON_CLICK_FX)
        sound.play()

class StatusBar(ButtonBehavior, BoxLayout):
    pass