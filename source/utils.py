def hex2rgba(hexcode):
    rgb = list(map(ord,hexcode[1:].decode('hex')))
    rgb.append(1)
    return tuple(rgb)