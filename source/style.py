
# color, in the format (red, green, blue, alpha).
# I like https://getbootstrap.com/docs/4.0/components/buttons/
# This will change the window clear color
from kivy.utils import get_color_from_hex
WHITE = "#ffffff"
GRAY_DARK = "#343a40"

PRIMARY_COLOR_HEX_CODE = "#007bff"
SECONDARY_COLOR_HEX_CODE = "#6c757d"
SUCCESS_COLOR_HEX_CODE = "#28a745"
DANGER_COLOR_HEX_CODE = "#dc3545"
INFO_COLOR_HEX_CODE = "#17a2b8"
LIGHT_COLOR_HEX_CODE = "#f8f9fa"
DARK_COLOR_HEX_CODE = "#343a40"



WINDOW_CLEAR_COLOR = get_color_from_hex(WHITE)

PRIMARY_BTN_COLOR = get_color_from_hex(PRIMARY_COLOR_HEX_CODE)
SECONDARY_BTN_COLOR = get_color_from_hex(SECONDARY_COLOR_HEX_CODE)
SUCCESS_BTN_COLOR = get_color_from_hex(SUCCESS_COLOR_HEX_CODE)

VOLUME_ICON_HIGH  = 'atlas://data/images/defaulttheme/audio-volume-high'
VOLUME_ICON_LOW  = 'atlas://data/images/defaulttheme/audio-volume-low'
VOLUME_ICON_MUTED  = 'atlas://data/images/defaulttheme/audio-volume-muted'


BUTTON_CLICK_FX = "assets/sound_fx/click3.wav"